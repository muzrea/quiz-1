package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_new_game() throws Exception {
        mockMvc.perform(post("/api/games").content("24")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201))
                .andExpect(header().stringValues("/api/games/24"));
    }

    @Test
    void should_get_correct_answer() throws Exception {
        mockMvc.perform(get("/api/games/23"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"gameId\":23,\"answer\":1243}"));
    }

    @Test
    void should_send_our_answer_and_verify_answer() throws Exception {
        HashSet<Game> gameDB = new HashSet<Game>();
        Game game23 = new Game();
        game23.setGameId(23);
        gameDB.add(game23);
        mockMvc.perform(patch("/api/games/23")
                .content("{ \"answer\": \"{1456}\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
}
