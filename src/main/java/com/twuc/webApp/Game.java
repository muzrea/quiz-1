package com.twuc.webApp;

import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class Game {
    private Integer gameId;
    private Integer answer;

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public void setAnswer(){
        Random random = new Random();
        Integer gameData = random.ints(1000,10000).findFirst().getAsInt();
        this.answer = gameData;
    }

    public Integer getGameId() {
        return gameId;
    }

    public Integer getAnswer() {
        return this.answer;
    }
}
