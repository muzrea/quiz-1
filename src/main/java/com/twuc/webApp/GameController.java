package com.twuc.webApp;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

import java.net.URI;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;

@RestController
@RequestMapping("/api")
public class GameController {
    @PostMapping("/games")
    public ResponseEntity<Game> newGamesAndLocations(@RequestBody Integer gameId){
        HashSet<Game> gameDB = new HashSet<Game>();
        boolean isIdDuplicated  = gameDB.stream().anyMatch(p -> p.getGameId().equals(gameId));
        if(isIdDuplicated){
            return ResponseEntity.unprocessableEntity().build();
        }
        Random random = new Random();
        Integer newGameId = random.ints(1,10000).findFirst().getAsInt();
        Game game = new Game();
        game.setGameId(newGameId);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{newGameId}")
                .buildAndExpand(newGameId)
                .toUri();
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.valueOf(location))
                .body(game);
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity<String> patchAnswerAndVerify(@PathVariable Integer gameId,@RequestBody Integer answer){
        Game game = getAnswer(gameId).getBody();
        String result = verifyAnswer(game.getAnswer(),answer);
        return ResponseEntity.status(HttpStatus.OK)
                .body(result);
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<Game> getAnswer(@PathVariable Integer gameId){
        Game game = new Game();
        game.setGameId(gameId);
        game.setAnswer(1243);
        return ResponseEntity.status(HttpStatus.OK)
                .body(game);
    }

    public String verifyAnswer(Integer myAnswer,Integer correctAnswer){
        int correctCount = 0;
        int count = 0;
        for(int i=0;i<5;i++){
            if(myAnswer.toString().charAt(i) ==correctAnswer.toString().charAt(i)){
                correctCount++;
            }
            for(int j=0;j<5;j++){
                if(myAnswer.toString().charAt(i) ==correctAnswer.toString().charAt(j)){
                    count++;
                }
            }
        }
        if(correctCount == 4 && count == 0){
            return "{ \"hint\": \"4A0B\", \"correct\": true }";
        }
        return "{ \"hint\": correctCount+\"A\"+count+\"B\", \"correct\": false }" ;
    }
}
